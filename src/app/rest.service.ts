import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'https://ni6p3sp4q8.execute-api.us-east-2.amazonaws.com/Producction/';

const httpOptions ={
  headers :new HttpHeaders({
    'Content-Type':'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }
  
  private extractData(res: Response){
    let body = res;
    return body || {};
  }

  doConversion(conversion): Observable<any>{
    console.log(conversion);
    return this.http.post<any>(endpoint + 'API_Conversion_FB', JSON.stringify(conversion), httpOptions).pipe(
      //tap((suma) => console.log(`La suma es : ${suma.data}`)),
      catchError(this.handleError<any>('doConversion'))
    );
  }
  
  private handleError<T> (operation ='operation', result?: T){
    return (error : any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of (result as T);
    };
  }

}
